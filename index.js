var express = require('express');
var Shipment = require('./controller/shipment-backend');
var app = express();
var cors = require('cors')


//body reader
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

app.use(cors())
/* 
//Get shipment quote
 */
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

app.post('/client/getquote', jsonParser, (req, res) => {
	req = req.body;
	Shipment.getQuote(req, res);
});

app.post('/client/createshipment', jsonParser, (req, res) => {
	req = req.body;
	Shipment.createShipment(req, res);
});

app.post('/client/getshipment', jsonParser, (req, res) => {
	req = req.body;
	Shipment.getShipment(req, res);
});

app.post('/client/deleteshipment', jsonParser, (req, res) => {
	req = req.body;
	Shipment.deleteShipment(req, res);
});

/* 
Create server
 */
app.listen(8081, () => {
	console.log("Example app listening on port 8081");
})

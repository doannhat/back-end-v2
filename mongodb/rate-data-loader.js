var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var file = "./database/rate.json"
var fs = require('fs');

function readRateData() {
    console.log("Start reading rate database");
    var content = fs.readFileSync(file, 'utf8');
    content = JSON.parse(content);
    return content;
}

MongoClient.connect(url, (err, db) => {
    if (err) throw new Error(err);
    let dbo = db.db('shipment-database');
    let content = readRateData();

    dbo.createCollection('rates', (err, res) => {
        if (err) throw new Error(err);
    })
    dbo.collection('rates').insertMany(content.rate, (err, res) => {
        if (err) throw new Error(err);
        console.log("Rates data created");
    })
    db.close();
})